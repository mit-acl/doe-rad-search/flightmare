#include <ros/ros.h>

#include "flightros/flight_swarm.hpp"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "flight_swarm");
    flightros::FlightSwarm swarm(ros::NodeHandle(), ros::NodeHandle("~"));
    
    ros::spin();

    return 0;
}
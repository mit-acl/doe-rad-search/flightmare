#include "flightros/dynamics_lite.h"

RigidBodyDynamics::RigidBodyDynamics() {}

void RigidBodyDynamics::loadParameters(const Matrix<double, 13, 1> &x0, const double& mass, const Matrix3d &inertia,
                                       const double& g)
{
    mass_ = mass;
    x_.arr = x0;
    inertia_matrix_ = inertia;
    inertia_inv_ = inertia_matrix_.inverse();
    g_ = g;
}

void RigidBodyDynamics::run(const double dt, const Vector4d& u, const Vector6d &ext_wrench)
{
    // 4th order Runge-Kutta integration
    f(x_, u, k1_, imu_, ext_wrench);

    x2_ = x_;
    x2_ += k1_ * (dt/2.0);
    f(x2_, u, k2_, ext_wrench);

    x3_ = x_;
    x3_ += k2_ * (dt/2.0);
    f(x3_, u, k3_, ext_wrench);

    x4_ = x_;
    x4_ += k3_ * dt;
    f(x4_, u, k4_, ext_wrench);

    dx_ = (k1_ + k2_*2.0 + k3_*2.0 + k4_) * (dt / 6.0);

    // Evolve state
    if (dx_.arr.norm() > 1.0e4) {
        dx_.arr.setZero();
        ROS_WARN("Huge dynamic update computed!");
    }
    x_ += dx_;
}

Vector3d RigidBodyDynamics::get_imu_accel() const 
{
    return imu_.segment<3>(ACC);
}

Vector3d RigidBodyDynamics::get_imu_gyro() const
{
    return imu_.segment<3>(GYRO);
}

void RigidBodyDynamics::f(const State& x, const Vector4d& ft, ErrorState& dx, const Vector6d &ext_wrench) const
{
    const Vector3d F_thr_B = ft(THRUST) * e_z;
    const Vector3d F_ext_B = ext_wrench.block<3,1>(0,0);
    const Vector3d T_thr_B = ft.segment<3>(TAUX);
    const Vector3d T_ext_B = ext_wrench.block<3,1>(3,0);

    dx.p = x.v;
    dx.v = -g_ * e_z + x.q.rota(F_thr_B + F_ext_B) / mass_;
    dx.q = x.w;
    dx.w = inertia_inv_ * (-x.w.cross(inertia_matrix_ * x.w) + T_thr_B + T_ext_B);
}

void RigidBodyDynamics::f(const State& x, const Vector4d& u, ErrorState& dx, Vector6d& imu, const Vector6d &ext_wrench) const
{
    f(x, u, dx, ext_wrench);
    imu.block<3,1>(ACC, 0) = dx.v + g_ * e_z; // Accelerometer measurement in FLU frame
    // imu.block<2,1>(ACC+1, 0) *= -1.0;         // FLU -> FRD for IMU frame
    imu.block<3,1>(GYRO, 0) = x.w;            // Rate Gyro measurement in FLU frame 
    // imu.block<2,1>(GYRO+1, 0) *= -1.0;        // FLU -> FRD for IMU frame
}

DynamicsLite::DynamicsLite() : nh_(), nh_private_("~"), uav_(), grav_(9.80665), taken_off_(false),
   input_((Vector4d() << 0., 0., 0., 0.).finished()), prev_time_sim_(-1.0), prev_time_motor_(-1.0), x0_(Xformd::Identity())
{
    loadParams();

    odom_pub_ = nh_.advertise<nav_msgs::Odometry>("odom_out", 1);
    pose_pub_ = nh_.advertise<geometry_msgs::PoseStamped>("pose_out", 1);
    imu_pub_  = nh_.advertise<sensor_msgs::Imu>("imu_out", 1);
    motor_speeds_sub_ = nh_.subscribe("motor_in", 1, &DynamicsLite::actuatorsCallback, this);

    timer_ = nh_.createTimer(ros::Duration(ros::Rate(1000)), &DynamicsLite::run, this);
}

void DynamicsLite::loadParams()
{
    // Load initial pose
    double px0, py0, pz0, qw0, qx0, qy0, qz0;
    px0 = nh_private_.param<double>("x_init", 0.0);
    py0 = nh_private_.param<double>("y_init", 0.0);
    pz0 = nh_private_.param<double>("z_init", 0.0);
    qw0 = nh_private_.param<double>("qw_init", 1.0);
    qx0 = nh_private_.param<double>("qx_init", 0.0);
    qy0 = nh_private_.param<double>("qy_init", 0.0);
    qz0 = nh_private_.param<double>("qz_init", 0.0);
    x0_.arr_ << px0, py0, pz0, qw0, qx0, qy0, qz0;

    // Load inertial parameters
    mass_ = nh_private_.param<double>("mass", 1.0);
    double Jx, Jy, Jz;
    Jx = nh_private_.param<double>("Jx", 0.01);
    Jy = nh_private_.param<double>("Jy", 0.01);
    Jz = nh_private_.param<double>("Jz", 0.01);
    inertia_ << Jx, 0.0, 0.0,
                0.0, Jy, 0.0,
                0.0, 0.0, Jz;

    // Load motor parameters
    std::vector<double> com;
    nh_private_.getParam("thrust_curve_coeff", thrust_curve_);
    nh_private_.getParam("torque_curve_coeff", torque_curve_);
    tau_spin_up_ = nh_private_.param<double>("tau_spin_up", 0.01);
    tau_spin_down_ = nh_private_.param<double>("tau_spin_down", 0.02);
    min_thrust_ = nh_private_.param<double>("min_thrust", 0.0);
    max_thrust_ = nh_private_.param<double>("max_thrust", 15.0);
    min_torque_ = nh_private_.param<double>("min_torque", 0.0);
    max_torque_ = nh_private_.param<double>("max_torque", 1.2);
    nh_private_.getParam("com", com);
    com_[0] = com[0];
    com_[1] = com[1];
    com_[2] = com[2];
    nh_private_.getParam("motor_positions", motor_positions_);
    nh_private_.getParam("motor_directions", motor_directions_);
    nh_private_.getParam("motor_spin", motor_spin_);
    
    // Load sensor parameters
    gyro_stdev_ = nh_private_.param<double>("gyro/stdev", 0.0);
    gyro_bias_walk_stdev_ = nh_private_.param<double>("gyro/bias_walk_stdev", 0.0);
    accel_stdev_ = nh_private_.param<double>("accel/stdev", 0.0);
    accel_bias_walk_stdev_ = nh_private_.param<double>("accel/bias_walk_stdev", 0.0);

    // Initialize dynamics
    uav_.loadParameters((Matrix<double, 13, 1>() << x0_.arr_, 0., 0., 0., 0., 0., 0.).finished(),
                        mass_, inertia_, grav_);

    // Construct allocation matrices
    wrench_from_motor_thrust_map_.resize(4, motor_spin_.size());
    wrench_from_motor_torque_map_.resize(4, motor_spin_.size());
    for (size_t i = 0; i < motor_spin_.size(); i++)
    {
        Vector3d direction(motor_directions_[3*i + 0],
                           motor_directions_[3*i + 1],
                           motor_directions_[3*i + 2]);
        direction.normalize();
        Vector3d position(motor_positions_[3*i + 0],
                          motor_positions_[3*i + 1],
                          motor_positions_[3*i + 2]);
        position -= com_;
        Vector3d body_moment_from_thrust = position.cross(direction);
        Vector3d body_moment_from_torque = -1.0 * motor_spin_[i] * direction;
        wrench_from_motor_thrust_map_(0,i) = direction.z();
        wrench_from_motor_thrust_map_.block<3,1>(1,i) = body_moment_from_thrust;
        wrench_from_motor_torque_map_(0,i) = 0.0;
        wrench_from_motor_torque_map_.block<3,1>(1,i) = body_moment_from_torque;
    }

    DLOG(INFO) << "\nThrust allocation matrix:\n";
    DLOG(INFO) << wrench_from_motor_thrust_map_;
    DLOG(INFO) << "\nTorque allocation matrix:\n";
    DLOG(INFO) << wrench_from_motor_torque_map_;
}

void DynamicsLite::actuatorsCallback(const mav_msgs::ActuatorsConstPtr &msg)
{
    DLOG_EVERY_N(INFO, 100) << "\nactuatorsCallback()";

    static Eigen::Matrix<double, Eigen::Dynamic, 1> des_motor_thrusts, des_motor_torques;
    static Eigen::Matrix<double, Eigen::Dynamic, 1> cur_motor_thrusts, cur_motor_torques;

    if (prev_time_motor_ < 0.0)
    {
        prev_time_motor_ = ros::Time::now().toSec();
        return;
    }

    double now_time = ros::Time::now().toSec();
    double dt = now_time - prev_time_motor_;
    prev_time_motor_ = now_time;

    DLOG_EVERY_N(INFO, 100) << "actuators: [" << msg->normalized[0] << ", " << msg->normalized[1]
                            << ", " << msg->normalized[2] << ", " << msg->normalized[3] << "]";

    // initialize data structures based on num actuators
    if (des_motor_thrusts.size() == 0) {
        const size_t m = motor_spin_.size();
        des_motor_thrusts = Eigen::MatrixXd::Zero(m, 1);
        des_motor_torques = Eigen::MatrixXd::Zero(m, 1);
        cur_motor_thrusts = Eigen::MatrixXd::Zero(m, 1);
        cur_motor_torques = Eigen::MatrixXd::Zero(m, 1);
    }

    // build two m-length vectors: motor trusts and motor torques
    for (size_t i=0; i<motor_spin_.size(); ++i) {
        // Motor pwm command
        const double u = msg->normalized[i];

        // map requested pwm into motor thrusts and torques
        des_motor_thrusts(i) = evalPoly(thrust_curve_, u);
        des_motor_torques(i) = evalPoly(torque_curve_, u);

        // filter through first-order spin up / spin down dynamics
        const double tau = (des_motor_thrusts(i) > cur_motor_thrusts(i)) ? tau_spin_up_ : tau_spin_down_;
        const double alpha = dt / (tau + dt);
        cur_motor_thrusts(i) = alpha*des_motor_thrusts(i) + (1-alpha)*cur_motor_thrusts(i);
        cur_motor_torques(i) = alpha*des_motor_torques(i) + (1-alpha)*cur_motor_torques(i);

        // clamp the output to respect actuator limits
        cur_motor_thrusts(i) = sat(cur_motor_thrusts(i), min_thrust_, max_thrust_);
        cur_motor_torques(i) = sat(cur_motor_torques(i), min_torque_, max_torque_);

    }

    // map motor thrusts and torques into body wrenches
    const Vector4d wrench_from_motor_thrusts = wrench_from_motor_thrust_map_ * cur_motor_thrusts;
    const Vector4d wrench_from_motor_torques = wrench_from_motor_torque_map_ * cur_motor_torques;

    input_ = wrench_from_motor_thrusts + wrench_from_motor_torques;

    DLOG_EVERY_N(INFO, 100) << "input calc: " << input_.transpose();
}

void DynamicsLite::run(const ros::TimerEvent&)
{
    static int counter = 0;
    DLOG_EVERY_N(INFO, 500) << "\nrun()";

    if (prev_time_sim_ < 0.0)
    {
        prev_time_sim_ = ros::Time::now().toSec();
        return;
    }

    double now_time = ros::Time::now().toSec();
    double dt = now_time - prev_time_sim_;
    prev_time_sim_ = now_time;

    if (!taken_off_ && abs(input_(THRUST)) < 1.01 * mass_ * grav_) // grounded, pre-takeoff
    {
        DLOG_EVERY_N(INFO, 500) << "grounded";
        input_.setZero();
        input_(THRUST) = mass_ * grav_;
    }
    else if (!taken_off_)
    {
        taken_off_ = true;
    }

    uav_.run(dt, input_);

    if (uav_.get_state().p(2) < 0.0 && abs(input_(THRUST)) < 1.01 * mass_ * grav_)
    {
        DLOG_EVERY_N(INFO, 500) << "ground collision reset";
        State grounded_state;
        (grounded_state.arr << x0_.arr_, 0., 0., 0., 0., 0., 0.).finished();
        uav_.set_state(grounded_state);
    }

    // Publish IMU
    sensor_msgs::Imu imu_msg;
    imu_msg.header.stamp = ros::Time::now();
    imu_msg.linear_acceleration.x = uav_.get_imu_accel().x();
    imu_msg.linear_acceleration.y = uav_.get_imu_accel().y();
    imu_msg.linear_acceleration.z = uav_.get_imu_accel().z();
    imu_msg.angular_velocity.x  = uav_.get_imu_gyro().x();
    imu_msg.angular_velocity.y  = uav_.get_imu_gyro().y();
    imu_msg.angular_velocity.z  = uav_.get_imu_gyro().z();
    imu_pub_.publish(imu_msg);

    DLOG_EVERY_N(INFO, 500) << "imu accel: " << uav_.get_imu_accel().transpose();
    DLOG_EVERY_N(INFO, 500) << "imu gyro:  " << uav_.get_imu_gyro().transpose();
    
    // Publish Odometry (NED -> NWU)
    nav_msgs::Odometry state_msg;
    state_msg.header.stamp = ros::Time::now();
    State current_state = uav_.get_state().toSnapConvention();
    state_msg.pose.pose.position.x = current_state.p.x();
    state_msg.pose.pose.position.y = current_state.p.y();
    state_msg.pose.pose.position.z = current_state.p.z();
    state_msg.pose.pose.orientation.w = current_state.q.w();
    state_msg.pose.pose.orientation.x = current_state.q.x();
    state_msg.pose.pose.orientation.y = current_state.q.y();
    state_msg.pose.pose.orientation.z = current_state.q.z();
    state_msg.twist.twist.linear.x = current_state.v.x();
    state_msg.twist.twist.linear.y = current_state.v.y();
    state_msg.twist.twist.linear.z = current_state.v.z();
    state_msg.twist.twist.angular.x = current_state.w.x();
    state_msg.twist.twist.angular.y = current_state.w.y();
    state_msg.twist.twist.angular.z = current_state.w.z();
    odom_pub_.publish(state_msg);

    DLOG_EVERY_N(INFO, 500) << "position: " << state_msg.pose.pose.position.x << " "
                                            << state_msg.pose.pose.position.y << " "
                                            << state_msg.pose.pose.position.z;
    DLOG_EVERY_N(INFO, 500) << "(NED) orientation: " << current_state.q.roll() << " "
                                               << current_state.q.pitch() << " "
                                               << current_state.q.yaw();
    DLOG_EVERY_N(INFO, 500) << "velocity: " << state_msg.twist.twist.linear.x << " "
                                            << state_msg.twist.twist.linear.y << " "
                                            << state_msg.twist.twist.linear.z;
    DLOG_EVERY_N(INFO, 500) << "angular velocity: " << state_msg.twist.twist.angular.x << " "
                                                    << state_msg.twist.twist.angular.y << " "
                                                    << state_msg.twist.twist.angular.z;

    if (counter >= 5)
    {
        counter = 0;
        geometry_msgs::PoseStamped pose_msg;
        pose_msg.header.stamp = ros::Time::now();
        pose_msg.pose = state_msg.pose.pose;
        pose_pub_.publish(pose_msg);

        geometry_msgs::TransformStamped transform_msg;
        transform_msg.header.stamp = ros::Time::now();
        transform_msg.header.frame_id = "snap_world";
        transform_msg.child_frame_id = "snap_quad";
        transform_msg.transform.translation.x = state_msg.pose.pose.position.x;
        transform_msg.transform.translation.y = state_msg.pose.pose.position.y;
        transform_msg.transform.translation.z = state_msg.pose.pose.position.z;
        transform_msg.transform.rotation.w = state_msg.pose.pose.orientation.w;
        transform_msg.transform.rotation.x = state_msg.pose.pose.orientation.x;
        transform_msg.transform.rotation.y = state_msg.pose.pose.orientation.y;
        transform_msg.transform.rotation.z = state_msg.pose.pose.orientation.z;
        tbr_.sendTransform(transform_msg);
    }
    counter++;
}
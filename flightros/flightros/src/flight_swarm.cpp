#include "flightros/flight_swarm.hpp"

namespace flightros {

FlightSwarm::FlightSwarm(const ros::NodeHandle& nh, const ros::NodeHandle& pnh)
: nh_(nh), pnh_(pnh), scene_id_(UnityScene::WAREHOUSE), unity_ready_(false), 
  unity_render_(false), num_vehicles_(1), receive_id_(0), main_loop_freq_(50.0)
{
    // load parameters
    if (!loadParams())
        ROS_FATAL("[%s] Could not load all parameters.", pnh_.getNamespace().c_str());
    else
        ROS_INFO("[%s] Loaded all parameters.", pnh_.getNamespace().c_str());

    // initialize ROS comms
    sub_state_est_ = nh_.subscribe("flight_swarm/state_estimates", 1, &FlightSwarm::statesCallback, this);
    timer_main_loop_ = nh_.createTimer(ros::Rate(main_loop_freq_), &FlightSwarm::mainLoopCallback, this);
    timer_camera_loop_ = nh_.createTimer(ros::Rate(camera_freq_), &FlightSwarm::cameraLoopCallback, this);
    timer_lidar_loop_ = nh_.createTimer(ros::Rate(lidar_freq_), &FlightSwarm::lidarLoopCallback, this);

    // wait until unity is loaded
    ros::Duration(5.0).sleep();

    // connect unity
    setUnity(unity_render_);
    connectUnity();
}

FlightSwarm::~FlightSwarm() {}

void FlightSwarm::statesCallback(const flightros_msgs::SwarmStates::ConstPtr &msg)
{
    ROS_ASSERT(msg->num_states == quad_states_.size());

    for (unsigned int i = 0; i < quad_states_.size(); i++)
    {
        quad_states_[i].x[QS::POSX] = (Scalar) msg->states[i].pose.pose.position.x;
        quad_states_[i].x[QS::POSY] = (Scalar) msg->states[i].pose.pose.position.y;
        quad_states_[i].x[QS::POSZ] = (Scalar) msg->states[i].pose.pose.position.z;
        quad_states_[i].x[QS::ATTW] = (Scalar) msg->states[i].pose.pose.orientation.w;
        quad_states_[i].x[QS::ATTX] = (Scalar) msg->states[i].pose.pose.orientation.x;
        quad_states_[i].x[QS::ATTY] = (Scalar) msg->states[i].pose.pose.orientation.y;
        quad_states_[i].x[QS::ATTZ] = (Scalar) msg->states[i].pose.pose.orientation.z;
    
        quad_ptrs_[i]->setState(quad_states_[i]);
    }
}

void FlightSwarm::mainLoopCallback(const ros::TimerEvent &event)
{
    if (unity_render_ && unity_ready_)
    {
        // Communicate with Unity
        unity_bridge_ptr_->getRender(0);
        unity_bridge_ptr_->handleOutput();
    }
}

void FlightSwarm::cameraLoopCallback(const ros::TimerEvent &event)
{
    if (unity_render_ && unity_ready_)
    {
        ros::Time synced_cam_time = ros::Time::now();
        for (int i = 0; i < rgb_cameras_.size(); i++)
        {
            camera_infos_[i].header.stamp = synced_cam_time;
            cv::Mat img_rgb;
            if (rgb_cameras_[i]->getRGBImage(img_rgb))
            {
                cv_bridge::CvImage msg;
                msg.header.stamp = synced_cam_time;
                msg.header.frame_id = camera_frames_[i];
                msg.encoding = sensor_msgs::image_encodings::BGR8;
                msg.image = img_rgb;
                camera_pubs_[i].publish(msg);
                camera_info_pubs_[i].publish(camera_infos_[i]);
            }

            cv::Mat img_depth, img_depth16;
            if (rgb_cameras_[i]->getDepthMap(img_depth))
            {
                const static float far = 5.0;
                const static float near = 0.3;

                cv::cvtColor(img_depth, img_depth, CV_BGR2GRAY);
                img_depth.convertTo(img_depth16, CV_16UC1, 1000.0*(far-near)/255.0);
                img_depth16 += 1000.0*near;
                
                // Convert CV mat to sensor_msgs::Image, the thorough way (bypassing cv_bridge).
                sensor_msgs::Image msg;
                msg.header.stamp = synced_cam_time;
                msg.header.frame_id = camera_frames_[i];
                msg.height = img_depth16.rows;
                msg.width = img_depth16.cols;
                msg.encoding = sensor_msgs::image_encodings::TYPE_16UC1;
                msg.is_bigendian = false; // (boost::endian::order::native == boost::endian::order::big);
                msg.step = img_depth16.cols * img_depth16.elemSize();
                size_t size = msg.step * img_depth16.rows;
                msg.data.resize(size);
                if (img_depth16.isContinuous())
                    memcpy((char*)(&msg.data[0]), img_depth16.data, size);
                else
                {
                    // Copy by row by row
                    uchar* ros_data_ptr = (uchar*)(&msg.data[0]);
                    uchar* cv_data_ptr = img_depth16.data;
                    for (int i = 0; i < img_depth16.rows; ++i)
                    {
                        memcpy(ros_data_ptr, cv_data_ptr, msg.step);
                        ros_data_ptr += msg.step;
                        cv_data_ptr += img_depth16.step;
                    }
                }
                depth_pubs_[i].publish(msg);
                depth_info_pubs_[i].publish(camera_infos_[i]);
            }
        }
    }
}

void FlightSwarm::lidarLoopCallback(const ros::TimerEvent &event)
{
    if (unity_render_ && unity_ready_)
    {
        for (int i = 0; i < lidar_indices_.size(); i++)
        {
            flightros_msgs::PointCloudFile msg;
            PointCloudMessage_t pointcloud_msg;
            pointcloud_msg.range = {lidar_info_[i][0],
                                    lidar_info_[i][1],
                                    lidar_info_[i][2]};
            pointcloud_msg.resolution = lidar_info_[i][3];
            pointcloud_msg.origin = {0.0,
                                     0.0,
                                     2.5};
            pointcloud_msg.path = "/tmp/";
            msg.header.stamp = ros::Time::now();
            pointcloud_msg.file_name = "lidar" + std::to_string(msg.header.stamp.toSec());
            msg.exists.data = unity_bridge_ptr_->getPointCloud(pointcloud_msg, 10.0 / lidar_freq_);
            msg.filename.data = pointcloud_msg.path + pointcloud_msg.file_name + ".ply";
            lidar_pubs_[i].publish(msg);
        }
    }
}

bool FlightSwarm::setUnity(const bool render) 
{
    unity_render_ = render;
    if (unity_render_ && unity_bridge_ptr_ == nullptr) 
    {
        // create unity bridge
        unity_bridge_ptr_ = UnityBridge::getInstance();
        for (unsigned int i = 0; i < quad_ptrs_.size(); i++)
            unity_bridge_ptr_->addQuadrotor(quad_ptrs_[i]);
        for (unsigned int i = 0; i < objects_.size(); i++)
            unity_bridge_ptr_->addStaticObject(objects_[i]);
        ROS_INFO("[%s] Unity Bridge is created.", pnh_.getNamespace().c_str());
  }
  return true;
}

bool FlightSwarm::connectUnity() 
{
    if (!unity_render_ || unity_bridge_ptr_ == nullptr) 
        return false;
    unity_ready_ = unity_bridge_ptr_->connectUnity(scene_id_);
    return unity_ready_;
}

bool FlightSwarm::loadParams(void)
{
    bool success = true;

    // load Unity info
    success &= pnh_.getParam("unity_scene_id", scene_id_);
    success &= pnh_.getParam("render_unity", unity_render_);
    success &= pnh_.getParam("main_loop_freq", main_loop_freq_);
    success &= pnh_.getParam("camera_freq", camera_freq_);
    success &= pnh_.getParam("lidar_freq", lidar_freq_); // ----

    // load Vehicle and Sensor info
    std::vector<std::string> cam_topic_names, depth_topic_names; 
    std::vector<double> veh_state_coeff, veh_cam_poses_coeff, veh_cam_info_coeff;
    success &= pnh_.getParam("num_vehicles", num_vehicles_);
    success &= pnh_.getParam("camera_topic_names", cam_topic_names);
    success &= pnh_.getParam("camera_frame_names", camera_frames_);
    success &= pnh_.getParam("depth_topic_names", depth_topic_names);
    success &= pnh_.getParam("initial_vehicle_poses", veh_state_coeff);
    success &= pnh_.getParam("vehicle_camera_poses", veh_cam_poses_coeff);
    success &= pnh_.getParam("vehicle_camera_info", veh_cam_info_coeff);

    // load Object info
    std::vector<std::string> obj_ids, obj_prefab_ids;
    std::vector<double> obj_pose_coeff, obj_size_coeff;
    success &= pnh_.getParam("num_objects", num_objects_);
    success &= pnh_.getParam("object_ids", obj_ids);
    success &= pnh_.getParam("object_prefab_ids", obj_prefab_ids);
    success &= pnh_.getParam("object_poses", obj_pose_coeff);
    success &= pnh_.getParam("object_sizes", obj_size_coeff);

    // construct vehicles, sensors, publishers from loaded information
    const int veh_pos_elem = 7;
    const int veh_cmp_elem = 7;
    const int veh_cmi_elem = 4; // quad_id, width, height, fov
    // const int veh_lid_elem = 5; // quad_id, range_x, range_y, range_z, resolution

    for (int i = 0; i < num_vehicles_; i++)
    {
        std::shared_ptr<Quadrotor> quad_ptr = std::make_shared<Quadrotor>();

        QuadState quad_state;
        quad_state.setZero();
        quad_state.x[QS::POSX] = (Scalar) veh_state_coeff[veh_pos_elem*i+0];
        quad_state.x[QS::POSY] = (Scalar) veh_state_coeff[veh_pos_elem*i+1];
        quad_state.x[QS::POSZ] = (Scalar) veh_state_coeff[veh_pos_elem*i+2];
        quad_state.x[QS::ATTW] = (Scalar) veh_state_coeff[veh_pos_elem*i+3];
        quad_state.x[QS::ATTX] = (Scalar) veh_state_coeff[veh_pos_elem*i+4];
        quad_state.x[QS::ATTY] = (Scalar) veh_state_coeff[veh_pos_elem*i+5];
        quad_state.x[QS::ATTZ] = (Scalar) veh_state_coeff[veh_pos_elem*i+6];

        quad_ptr->reset(quad_state);
        quad_ptrs_.push_back(quad_ptr);
        quad_states_.push_back(quad_state);
    }

    for (int i = 0; i < cam_topic_names.size(); i++)
    {
        std::shared_ptr<RGBCamera> rgb_camera = std::make_shared<RGBCamera>();
        Vector<3> cam_rpos(veh_cam_poses_coeff[veh_cmp_elem*i+0],
                           veh_cam_poses_coeff[veh_cmp_elem*i+1],
                           veh_cam_poses_coeff[veh_cmp_elem*i+2]);
        Matrix<3,3> cam_ratt = Quaternion(veh_cam_poses_coeff[veh_cmp_elem*i+3],
                                          veh_cam_poses_coeff[veh_cmp_elem*i+4],
                                          veh_cam_poses_coeff[veh_cmp_elem*i+5],
                                          veh_cam_poses_coeff[veh_cmp_elem*i+6]).toRotationMatrix();
        rgb_camera->setRelPose(cam_rpos, cam_ratt);

        double width = veh_cam_info_coeff[veh_cmi_elem*i+1];
        double height = veh_cam_info_coeff[veh_cmi_elem*i+2];
        double fov = veh_cam_info_coeff[veh_cmi_elem*i+3];

        rgb_camera->setWidth(width);
        rgb_camera->setHeight(height);
        rgb_camera->setFOV(fov);
        rgb_camera->setPostProcessing(std::vector<bool>{true, false, false});

        quad_ptrs_[static_cast<int>(veh_cam_info_coeff[veh_cmi_elem*i+0])]->addRGBCamera(rgb_camera);
        rgb_cameras_.push_back(rgb_camera);

        ros::Publisher cam_publisher = nh_.advertise<sensor_msgs::Image>(cam_topic_names[i], 1);
        camera_pubs_.push_back(cam_publisher);

        sensor_msgs::CameraInfo rgb_info;
        rgb_info.header.frame_id = cam_topic_names[i];
        rgb_info.height = static_cast<uint32_t>(height);
        rgb_info.width  = static_cast<uint32_t>(width);
        rgb_info.distortion_model = "plumb_bob";
        rgb_info.D = {0.0, 0.0, 0.0, 0.0, 0.0};
        double fx = width / (2.0 * tanf(M_PI * fov / 180.0 / 2.0));
        double fy = height / (2.0 * tanf(M_PI * fov / 180.0 / 2.0));
        rgb_info.K[0] = fx;
        rgb_info.K[1] = 0.;
        rgb_info.K[2] = width / 2.0;
        rgb_info.K[3] = 0.;
        rgb_info.K[4] = fy;
        rgb_info.K[5] = height / 2.0;
        rgb_info.K[6] = 0.;
        rgb_info.K[7] = 0.;
        rgb_info.K[8] = 1.;
        rgb_info.R[0] = 1.;
        rgb_info.R[1] = 0.;
        rgb_info.R[2] = 0.;
        rgb_info.R[3] = 0.;
        rgb_info.R[4] = 1.;
        rgb_info.R[5] = 0.;
        rgb_info.R[6] = 0.;
        rgb_info.R[7] = 0.;
        rgb_info.R[8] = 1.;
        rgb_info.P[0] = rgb_info.K[0];
        rgb_info.P[1] = rgb_info.K[1];
        rgb_info.P[2] = rgb_info.K[2];
        rgb_info.P[3] = 0.;
        rgb_info.P[4] = rgb_info.K[3];
        rgb_info.P[5] = rgb_info.K[4];
        rgb_info.P[6] = rgb_info.K[5];
        rgb_info.P[7] = 0.;
        rgb_info.P[8] = rgb_info.K[6];
        rgb_info.P[9] = rgb_info.K[7];
        rgb_info.P[10] = rgb_info.K[8];
        rgb_info.P[11] = 0.;
        camera_infos_.push_back(rgb_info);

        std::stringstream cam_info_topic_name;
        cam_info_topic_name << cam_topic_names[i] << "/info";
        ros::Publisher cam_info_publisher = nh_.advertise<sensor_msgs::CameraInfo>(cam_info_topic_name.str(), 1);
        camera_info_pubs_.push_back(cam_info_publisher);
        
        ros::Publisher depth_publisher = nh_.advertise<sensor_msgs::Image>(depth_topic_names[i], 1);
        depth_pubs_.push_back(depth_publisher);

        std::stringstream depth_info_topic_name;
        depth_info_topic_name << depth_topic_names[i] << "/info";
        ros::Publisher depth_info_publisher = nh_.advertise<sensor_msgs::Image>(depth_info_topic_name.str(), 1);
        depth_info_pubs_.push_back(depth_info_publisher);
    }

    // Construct static objects
    const int obj_pose_el = 7;
    const int obj_size_el = 3;

    for (int i = 0; i < num_objects_; i++)
    {
        std::shared_ptr<StaticGate> obj_ptr = std::make_shared<StaticGate>(obj_ids[i], obj_prefab_ids[i]);
        obj_ptr->setPosition(Vector<3>(obj_pose_coeff[obj_pose_el*i+0],
                                       obj_pose_coeff[obj_pose_el*i+1],
                                       obj_pose_coeff[obj_pose_el*i+2]));
        obj_ptr->setRotation(Quaternion(obj_pose_coeff[obj_pose_el*i+3],
                                        obj_pose_coeff[obj_pose_el*i+4],
                                        obj_pose_coeff[obj_pose_el*i+5],
                                        obj_pose_coeff[obj_pose_el*i+6]));
        obj_ptr->setSize(Vector<3>(obj_size_coeff[obj_size_el*i+0],
                                   obj_size_coeff[obj_size_el*i+1],
                                   obj_size_coeff[obj_size_el*i+2]));
        objects_.push_back(obj_ptr);
    }

    return success;
}

} // end namespace flightros
#include "flightros/dynamics_lite.h"

DEFINE_bool(create_log, false, "Whether or not to create a log.");

int main(int argc, char** argv)
{
    ros::init(argc, argv, "dynamics_lite_node");
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    if (FLAGS_create_log)
    {
        google::InstallFailureSignalHandler();
        DLOG(INFO) << "[DEBUG MODE]";
        std::cout << "";
    }
    else
    {
        google::SetCommandLineOption("GLOG_minloglevel", "2");
    }
    
    DynamicsLite DL;
    ros::spin();
    return 0;
}
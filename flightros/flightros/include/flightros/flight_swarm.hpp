#pragma once

#include <memory>

#include <flightros_msgs/SwarmStates.h>
#include <flightros_msgs/PointCloudFile.h>
#include <ros/ros.h>

#include "flightlib/bridges/unity_bridge.hpp"
#include "flightlib/common/quad_state.hpp"
#include "flightlib/common/types.hpp"
#include "flightlib/objects/quadrotor.hpp"
#include "flightlib/objects/static_gate.hpp"
#include "flightlib/sensors/rgb_camera.hpp"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>

using namespace flightlib;

namespace flightros {

class FlightSwarm 
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    FlightSwarm(const ros::NodeHandle& nh, const ros::NodeHandle& pnh);
    ~FlightSwarm();

    // callbacks
    void mainLoopCallback(const ros::TimerEvent& event);
    void cameraLoopCallback(const ros::TimerEvent& event);
    void lidarLoopCallback(const ros::TimerEvent& event);
    void statesCallback(const flightros_msgs::SwarmStates::ConstPtr& msg);

    bool setUnity(const bool render);
    bool connectUnity(void);
    bool loadParams(void);

private:
    // ros nodes
    ros::NodeHandle nh_;
    ros::NodeHandle pnh_;

    // publisher
    std::vector<ros::Publisher> camera_pubs_;
    std::vector<ros::Publisher> depth_pubs_;
    std::vector<ros::Publisher> camera_info_pubs_;
    std::vector<ros::Publisher> depth_info_pubs_;
    std::vector<sensor_msgs::CameraInfo> camera_infos_;
    std::vector<std::string> camera_frames_;
    std::vector<ros::Publisher> lidar_pubs_;

    // subscriber
    ros::Subscriber sub_state_est_;

    // loop timers
    ros::Timer timer_main_loop_;
    ros::Timer timer_camera_loop_;
    ros::Timer timer_lidar_loop_;

    // unity quadrotor
    std::vector<std::shared_ptr<Quadrotor>> quad_ptrs_;
    std::vector<int> lidar_indices_;
    std::vector<std::vector<double>> lidar_info_;
    std::vector<std::shared_ptr<RGBCamera>> rgb_cameras_;
    std::vector<QuadState> quad_states_;

    // unity objects
    std::vector<std::shared_ptr<StaticGate>> objects_;

    // Flightmare(Unity3D)
    std::shared_ptr<UnityBridge> unity_bridge_ptr_;
    // SceneID scene_id_{UnityScene::WAREHOUSE};
    int scene_id_;
    bool unity_ready_;
    bool unity_render_;
    RenderMessage_t unity_output_;
    int num_vehicles_;
    int num_objects_;
    uint16_t receive_id_;

    // auxiliary variables
    Scalar main_loop_freq_;
    Scalar camera_freq_;
    Scalar lidar_freq_;
};

} // end namespace flightros
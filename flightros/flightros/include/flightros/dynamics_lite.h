#pragma once

#include <ros/ros.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <nav_msgs/Odometry.h>
#include <mav_msgs/Actuators.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Imu.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <math.h>
#include <iostream>

#include <glog/logging.h>
#include <gflags/gflags.h>

using namespace Eigen;

static const Matrix<double,3,2> I_3x2 = [] {
  Matrix<double,3,2> tmp;
  tmp << 1, 0, 0, 1, 0, 0;
  return tmp;
}();

static const Vector3d e1 = [] {
  Vector3d tmp;
  tmp << 1, 0, 0;
  return tmp;
}();

static const Vector3d e2 = [] {
  Vector3d tmp;
  tmp << 0, 1, 0;
  return tmp;
}();

static const Vector3d e3 = [] {
  Vector3d tmp;
  tmp << 0, 0, 1;
  return tmp;
}();

template<typename T>
class Quat
{

private:
  typedef Matrix<T,4,1> Vec4;
  typedef Matrix<T,3,1> Vec3;
  T buf_[4];

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Quat() :
    arr_(buf_)
  {}

  Quat(const Ref<const Vec4> arr) :
    arr_(const_cast<T*>(arr.data()))
  {}

  Quat(const Quat& q)
    : arr_(buf_)
  {
    arr_ = q.arr_;
  }

  Quat(const T* data) :
    arr_(const_cast<T*>(data))
  {}

  inline T* data() { return arr_.data(); }

  Map<Vec4> arr_;

  inline T& operator[] (int i) {return arr_[i];}
  inline T w() const { return arr_(0); }
  inline T x() const { return arr_(1); }
  inline T y() const { return arr_(2); }
  inline T z() const { return arr_(3); }
  inline void setW(T w) { arr_(0) = w; }
  inline void setX(T x) { arr_(1) = x; }
  inline void setY(T y) { arr_(2) = y; }
  inline void setZ(T z) { arr_(3) = z; }
  inline const Vec4 elements() const { return arr_;}
  inline const T* data() const { return arr_.data();}


  template<typename T2>
  Quat operator* (const Quat<T2>& q) const { return otimes(q); }
  Quat& operator *= (const Quat& q)
  {
    arr_ << w() * q.w() - x() *q.x() - y() * q.y() - z() * q.z(),
            w() * q.x() + x() *q.w() + y() * q.z() - z() * q.y(),
            w() * q.y() - x() *q.z() + y() * q.w() + z() * q.x(),
            w() * q.z() + x() *q.y() - y() * q.x() + z() * q.w();
  }

  Quat& operator= (const Quat& q) { arr_ = q.elements(); }

  template<typename Derived>
  Quat& operator= (MatrixBase<Derived> const& in) {arr_ = in; }

  Quat operator+ (const Vec3& v) const { return boxplus(v); }
  Quat& operator+= (const Vec3& v)
  {
    arr_ = boxplus(v).elements();
  }

  template<typename T2>
  Matrix<T,3,1> operator- (const Quat<T2>& q) const {return boxminus(q);}

  static Matrix<T,3,3> skew(const Vec3& v)
  {
    static Matrix<T,3,3> skew_mat;
    skew_mat << (T)0.0, -v(2), v(1),
                v(2), (T)0.0, -v(0),
                -v(1), v(0), (T)0.0;
    return skew_mat;
  }

//  template<typename T2>
//  Quat<T2> cast() const
//  {
//    return quat::Quat<T2>(arr_.cast<T2>());
//  }

  static Quat exp(const Vec3& v)
  {
    T norm_v = v.norm();

    Quat q;
    if (norm_v > 1e-4)
    {
      T v_scale = sin(norm_v/2.0)/norm_v;
      q.arr_ << cos(norm_v/2.0), v_scale*v(0), v_scale*v(1), v_scale*v(2);
    }
    else
    {
      q.arr_ << (T)1.0, v(0)/2.0, v(1)/2.0, v(2)/2.0;
      q.arr_ /= q.arr_.norm();
    }
    return q;
  }

  static Vec3 log(const Quat& q)
  {
    Vec3 v = q.arr_.template block<3,1>(1,0);
    T w = q.w();
    T norm_v = v.norm();

    Vec3 out;
    if (norm_v < (T)1e-8)
    {
      out.setZero();
    }
    else
    {
      out = (T)2.0*atan2(norm_v, w)*v/norm_v;
    }
    return out;
  }

  static Quat from_R(const Matrix<T,3,3>& m)
  {
    Quat q;
    T tr = m.trace();

    if (tr > 0)
    {
      T S = sqrt(tr+1.0) * 2.;
      q.arr_ << 0.25 * S,
               (m(1,2) - m(2,1)) / S,
               (m(2,0) - m(0,2)) / S,
               (m(0,1) - m(1,0)) / S;
    }
    else if ((m(0,0) > m(1,1)) && (m(0,0) > m(2,2)))
    {
      T S = sqrt(1.0 + m(0,0) - m(1,1) - m(2,2)) * 2.;
      q.arr_ << (m(1,2) - m(2,1)) / S,
                0.25 * S,
                (m(1,0) + m(0,1)) / S,
                (m(2,0) + m(0,2)) / S;
    }
    else if (m(1,1) > m(2,2))
    {
      T S = sqrt(1.0 + m(1,1) - m(0,0) - m(2,2)) * 2.;
      q.arr_ << (m(2,0) - m(0,2)) / S,
                (m(1,0) + m(0,1)) / S,
                0.25 * S,
                (m(2,1) + m(1,2)) / S;
    }
    else
    {
      T S = sqrt(1.0 + m(2,2) - m(0,0) - m(1,1)) * 2.;
      q.arr_ << (m(0,1) - m(1,0)) / S,
                (m(2,0) + m(0,2)) / S,
                (m(2,1) + m(1,2)) / S,
                0.25 * S;
    }
    return q;
  }

  static Quat from_axis_angle(const Vec3& axis, const T angle)
  {
    T alpha_2 = angle/2.0;
    T sin_a2 = sin(alpha_2);
    Quat out;
    out.arr_(0) = cos(alpha_2);
    out.arr_(1) = axis(0)*sin_a2;
    out.arr_(2) = axis(1)*sin_a2;
    out.arr_(3) = axis(2)*sin_a2;
    out.arr_ /= out.arr_.norm();
    return out;
  }

  static Quat from_euler(const T roll, const T pitch, const T yaw)
  {
    T cp = cos(roll/2.0);
    T ct = cos(pitch/2.0);
    T cs = cos(yaw/2.0);
    T sp = sin(roll/2.0);
    T st = sin(pitch/2.0);
    T ss = sin(yaw/2.0);

    Quat q;
    q.arr_ << cp*ct*cs + sp*st*ss,
              sp*ct*cs - cp*st*ss,
              cp*st*cs + sp*ct*ss,
              cp*ct*ss - sp*st*cs;
    return q;
  }

  static Quat from_two_unit_vectors(const Vec3& u, const Vec3& v)
  {
    Quat q_out;

    T d = u.dot(v);
    if (d < 0.99999999 && d > -0.99999999)
    {
      T invs = 1.0/sqrt((2.0*(1.0+d)));
      Vec3 xyz = u.cross(v*invs);
      q_out.arr_(0) = 0.5/invs;
      q_out.arr_.template block<3,1>(1,0)=xyz;
      q_out.arr_ /= q_out.arr_.norm();
    }
    else if (d < -0.99999999)
    {
        // TODO Questionable logic...
//      q_out.arr_ << (T)0, (T)1, (T)0, (T)0; // There are an infinite number of solutions here, choose one
        q_out.arr_ << (T)0, (T)0, (T)1, (T)0; // This choice works better for vector comparisons with only nonzero x components (as I like to do with arrows...)
    }
    else
    {
      q_out.arr_ << (T)1, (T)0, (T)0, (T)0;
    }
    return q_out;
  }

  static Quat Identity()
  {
    Quat q;
    q.arr_ << 1.0, 0, 0, 0;
    return q;
  }

  static Quat Random()
  {
    Quat q_out;
    q_out.arr_.setRandom();
    q_out.arr_ /= q_out.arr_.norm();
    return q_out;
  }

  T roll() const
  {
    return atan2(T(2.0)*(w()*x() + y()*z()), T(1.0) - T(2.0)*(x()*x() + y()*y()));
  }

  T pitch() const
  {
    const T val = T(2.0) * (w()*y() - x()*z());

    // hold at 90 degrees if invalid
    if (fabs(val) > T(1.0))
      return copysign(T(1.0), val) * T(M_PI) / T(2.0);
    else
      return asin(val);
  }

  T yaw() const
  {
    return atan2(T(2.0)*(w()*z() + x()*y()), T(1.0) - T(2.0)*(y()*y() + z()*z()));
  }

  Vec3 euler() const
  {
    Vec3 out;
    out << roll(), pitch(), yaw();
    return out;
  }

  Vec3 bar() const
  {
    return arr_.template block<3,1>(1,0);
  }

  Matrix<T,3,3> R() const
  {
    T wx = w()*x();
    T wy = w()*y();
    T wz = w()*z();
    T xx = x()*x();
    T xy = x()*y();
    T xz = x()*z();
    T yy = y()*y();
    T yz = y()*z();
    T zz = z()*z();
    Matrix<T,3,3> out;
    out << 1. - 2.*yy - 2.*zz, 2.*xy + 2.*wz,      2.*xz - 2.*wy,
           2.*xy - 2.*wz,      1. - 2.*xx - 2.*zz, 2.*yz + 2.*wx,
           2.*xz + 2.*wy,      2.*yz - 2.*wx,      1. - 2.*xx - 2.*yy;
    return out;
  }

  Quat copy() const
  {
    Quat tmp;
    tmp.arr_ = arr_;
    return tmp;
  }

  void normalize()
  {
    arr_ /= arr_.norm();
  }

  Matrix<T, 3, 2> doublerota(const Matrix<T, 3, 2>& v) const
  {
    Matrix<T, 3, 2> out(3, 2);
    Vec3 t;
    for (int i = 0; i < 2; ++i)
    {
      t = 2.0 * v.col(i).cross(bar());
      out.col(i) = v.col(i) - w() * t + t.cross(bar());
    }
    return out;
  }

  Matrix<T, 3, 2> doublerotp(const Matrix<T, 3, 2>& v) const
  {
    Matrix<T, 3, 2> out(3, 2);
    Vec3 t;
    for (int i = 0; i < 2; ++i)
    {
      t = 2.0 * v.col(i).cross(bar());
      out.col(i) = v.col(i) + w() * t + t.cross(bar());
    }
    return out;
  }


  // The same as R.T * v but faster
  template<typename Tout=T, typename T2>
  Matrix<Tout, 3, 1> rota(const Matrix<T2,3,1>& v) const
  {
    Matrix<Tout, 3, 1> t = (Tout)2.0 * v.cross(bar());
    return v - w() * t + t.cross(bar());
  }

  Vec3 rota(const Vec3& v) const
  {
    Vec3 t = 2.0 * v.cross(bar());
    return v - w() * t + t.cross(bar());
  }

  // The same as R * v but faster
  template<typename T2>
  Vec3 rotp(const Matrix<T2, 3, 1>& v) const
  {
    Vec3 t = 2.0 * v.cross(bar());
    return v + w() * t + t.cross(bar());
  }

  Vec3 rotp(const Vec3& v) const
  {
      Vec3 t = 2.0 * v.cross(bar());
      return v + w() * t + t.cross(bar());
  }

  Quat& invert()
  {
    arr_.template block<3,1>(1,0) *= (T)-1.0;
  }

  Quat inverse() const
  {
    Quat tmp;
    tmp.arr_(0) = arr_(0);
    tmp.arr_(1) = -arr_(1);
    tmp.arr_(2) = -arr_(2);
    tmp.arr_(3) = -arr_(3);
    return tmp;
  }

//  template <typename T2>
//  Quat otimes(const Quat<T2>& q) const
//  {
//    Quat qout;
//    qout.arr_ <<  w() * q.w() - x() *q.x() - y() * q.y() - z() * q.z(),
//                  w() * q.x() + x() *q.w() + y() * q.z() - z() * q.y(),
//                  w() * q.y() - x() *q.z() + y() * q.w() + z() * q.x(),
//                  w() * q.z() + x() *q.y() - y() * q.x() + z() * q.w();
//    return qout;
//  }

  template <typename Tout=T, typename T2>
  Quat<Tout> otimes(const Quat<T2>& q) const
  {
      Quat<Tout> qout;
      qout.arr_ <<  w() * q.w() - x() *q.x() - y() * q.y() - z() * q.z(),
                    w() * q.x() + x() *q.w() + y() * q.z() - z() * q.y(),
                    w() * q.y() - x() *q.z() + y() * q.w() + z() * q.x(),
                    w() * q.z() + x() *q.y() - y() * q.x() + z() * q.w();
      return qout;
  }

  template<typename Tout=T, typename T2>
  Quat<Tout> boxplus(const Matrix<T2, 3, 1>& delta) const
  {
    return otimes<Tout, T2>(Quat<T2>::exp(delta));
  }

  template<typename Tout=T, typename T2>
  Matrix<Tout, 3, 1> boxminus(const Quat<T2> &q) const
  {
    Quat<Tout> dq = q.inverse().template otimes<Tout>(*this);
    if (dq.w() < 0.0)
    {
      dq.arr_ *= (Tout)-1.0;
    }
    return Quat<Tout>::log(dq);
  }

  Matrix<T,3,1> uvec() const
  {
    return inverse().rotp(e3.cast<T>());
  }

  // Get projection from 2D space orthogonal to unit vector
  Matrix<T,3,2> proj() const
  {
    return inverse().R() * I_3x2.cast<T>();
  }

  // q1 - q2
  // logarithmic map given two quaternions representing unit vectors
  template <typename T2, typename T3>
  static Matrix<T,2,1> log_uvec(const Quat<T2>& q1, const Quat<T3>& q2)
  {
    // get unit vectors
    Matrix<T2,3,1> e1 = q1.uvec();
    Matrix<T3,3,1> e2 = q2.uvec();

    // avoid too small of angles
    T e2T_e1 = e2.dot(e1);
    if (e2T_e1 > T(0.999999))
      return Matrix<T,2,1>(T(0.0), T(0.0));
    else if (e2T_e1 < T(-0.999999))
      return Matrix<T,2,1>(T(M_PI), T(0.0));
    else
    {
      // compute axis angle difference
      Matrix<T,3,1> e2_x_e1 = e2.cross(e1);
      Matrix<T,3,1> s = acos(e2T_e1) * e2_x_e1.normalized();

      // place error on first vector's tangent space
      return q1.proj().transpose() * s;
    }
  }

};

template<typename T>
inline std::ostream& operator<< (std::ostream& os, const Quat<T>& q)
{
  os << "[ " << q.w() << ", " << q.x() << "i, " << q.y() << "j, " << q.z() << "k]";
  return os;
}

typedef Quat<double> Quatd;

template <typename T>
class Xform
{
private:

  typedef Matrix<T, 2, 1> Vec2;
  typedef Matrix<T, 3, 1> Vec3;
  typedef Matrix<T, 4, 1> Vec4;
  typedef Matrix<T, 5, 1> Vec5;
  typedef Matrix<T, 6, 1> Vec6;
  typedef Matrix<T, 7, 1> Vec7;

  typedef Matrix<T, 3, 3> Mat3;
  typedef Matrix<T, 4, 4> Mat4;
  typedef Matrix<T, 6, 6> Mat6;
  T buf_[7];

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Map<Vec7> arr_;
  Map<Vec3> t_;
  Quat<T> q_;

  Xform() :
    arr_(buf_),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {}

  Xform(const Ref<const Vec7> arr) :
    arr_(const_cast<T*>(arr.data())),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {}

  Xform(const Xform& X) :
    arr_(buf_),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {
    arr_ = X.arr_;
  }

  Xform(const T* data) :
    arr_(const_cast<T*>(data)),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {}

  Xform(const Vec3& t, const Quat<T>& q) :
    arr_(buf_),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {
    // copies arguments into contiguous (owned) memory
    t_ = t;
    q_ = q;
  }

  Xform(const Vec3& t, const Mat3& R) :
    arr_(buf_),
    t_(arr_.data()),
    q_(arr_.data() + 3)
  {
    q_ = Quat<T>::from_R(R);
    t_ = t;
  }

  T* data()
  {
      return arr_.data();
  }

//  Xform(const Mat4& X) :
//    arr_(buf_),
//    t_(arr_.data()),
//    q_(arr_.data() + 3)
//  {
//    q_ = Quat<T>::from_R(X.block<3,3>(0,0));
//    t_ = X.block<3,1>(0, 3);
//  }

  inline Map<Vec3>& t() { return t_;}
  inline Quat<T>& q() { return q_;}
  inline Map<Vec7>& arr() { return arr_; }
  inline void setq(const Quat<T>& q) {q_ = q;}
  inline void sett(const Vec3&t) {t_ = t;}

  Xform operator* (const Xform& X) const {return otimes(X);}
  Xform& operator*= (const Xform& X)
  {
    t_ = t_ + q_.rotp(X.t_);
    q_ = q_ * X.q_;
  }
  Xform& operator=(const Xform& X) {t_ = X.t_; q_ = X.q_;}
  Xform& operator=(const Vec7& v) {
    t_ = v.template segment<3>(0);
    q_ = Quat<T>(v.template segment<4>(3));
  }

  Xform operator+ (const Vec6& v) const
  {
    return boxplus(v);
  }

  template<typename T2>
  Matrix<T2,6,1> operator- (const Xform<T2>& X) const
  {
    return boxminus(X);
  }

  Xform& operator+=(const Vec6& v)
  {
    *this = boxplus(v);
  }

  Vec7 elements() const
  {
    Vec7 out;
    out.template block<3,1>(0,0) = t_;
    out.template block<4,1>(3,0) = q_.arr_;
    return out;
  }

  // Mat4 Mat() const // just wrong... this is trying to make the inverse of what the
  //                  // default, passive matrix should be, but even then it messed up
  //                  // the rotation, which would be q_.R().transpose()
  // {
  //   Mat4 out;
  //   out.template block<3,3>(0,0) = q_.R();
  //   out.template block<3,1>(0,3) = t_;
  //   out.template block<1,3>(3,0) = Matrix<T,1,3>::Zero();
  //   out(3,3) = 1.0;
  //   return out;
  // }

  Mat4 H() const
  {
    Mat4 out;
    out.template block<3,3>(0,0) = q_.R();
    out.template block<3,1>(0,3) = -q_.R() * t_;
    out.template block<1,3>(3,0) = Matrix<T,1,3>::Zero();
    out(3,3) = 1.0;
    return out;
  }

  static Xform Identity()
  {
    Xform out;
    out.t_.setZero();
    out.q_ = Quat<T>::Identity();
    return out;
  }

  Xform relativeTo(const Xform &other)
  {
    return Xform::Identity().boxplus(boxminus(other));
  }

  static Xform Random()
  {
    Xform out;
    out.t_.setRandom();
    out.q_ = Quat<T>::Random();
    return out;
  }

  static Xform exp(const Vec6& v)
  {
    Vec3 u = v.template block<3,1>(0,0);
    Vec3 omega = v.template block<3,1>(3,0);
    T th = omega.norm();
    Quat<T> q_exp = Quat<T>::exp(omega);
    if (th > 1e-4)
    {
      Mat3 wx = Quat<T>::skew(omega);
      T B = ((T)1. - cos(th)) / (th * th);
      T C = (th - sin(th)) / (th * th * th);
      return Xform((Matrix3d::Identity() + B*wx + C*wx*wx).transpose() * u, q_exp);
    }
    else
    {
      return Xform(u, q_exp);
    }
  }

  static Vec6 log(const Xform& X)
  {
    Vec6 u;
    Vec3 omega = Quat<T>::log(X.q_);
    u.template block<3,1>(3,0) = omega;
    T th = omega.norm();
    if (th > 1e-8)
    {
      Mat3 wx = Quat<T>::skew(omega);
      T A = sin(th)/th;
      T B = ((T)1. - cos(th)) / (th * th);
      Mat3 V = Matrix3d::Identity() - (1./2.)*wx + (1./(th*th)) * (1.-(A/(2.*B)))*(wx* wx);
      u.template block<3,1>(0,0) = V.transpose() * X.t_;
    }
    else
    {
      u.template block<3,1>(0,0) = X.t_;
    }
    return u;
  }

  Mat6 Adj() const
  {
    Mat6 out;
    Mat3 R = q_.R();
    out.template block<3,3>(0,0) = R;
    out.template block<3,3>(0,3) = Quat<T>::skew(t_)*R;
    out.template block<3,3>(3,3) = R;
    out.template block<3,3>(3,0) = Mat3::Zero();
    return out;
  }

  Xform inverse() const{
    Xform out(-q_.rotp(t_), q_.inverse());
    return out;
  }

  template <typename Tout=T, typename T2>
  Xform<Tout> otimes(const Xform<T2>& X2) const
  {
    Xform<Tout> X;
    Matrix<Tout,3,1> t = (Tout)2.0*X2.t_.cross(q_.bar());
    X.t_ = t_+ X2.t_ - q_.w()* t + t.cross(q_.bar());
    X.q_ = q_.template otimes<Tout,T2>(X2.q_);
    return X;
  }

  Vec3 transforma(const Vec3& v) const
  {
    return q_.rota(v) + t_;
  }

  Vec3 transformp(const Vec3& v) const
  {
    return q_.rotp(v - t_);
  }

  Xform& invert()
  {
    t_ = -q_.rotp(t_);
    q_.invert();
  }

  template <typename Tout=T, typename T2>
  Xform<Tout> boxplus(const Matrix<T2, 6, 1>& delta) const
  {
    return otimes<Tout, T2>(Xform<T2>::exp(delta));
  }

  template<typename T2>
  Matrix<T2,6,1> boxminus(const Xform<T2>& X) const
  {
    return Xform<T2>::log(X.inverse().otimes(*this));
  }

};

template <typename T>
inline std::ostream& operator<< (std::ostream& os, const Xform<T>& X)
{
  os << "t: [ " << X.t_(0,0) << ", " << X.t_(1,0) << ", " << X.t_(2,0) <<
        "] q: [ " << X.q_.w() << ", " << X.q_.x() << "i, " << X.q_.y() << "j, " << X.q_.z() << "k]";
  return os;
}

typedef Xform<double> Xformd;

typedef Matrix<double, 6, 1> Vector6d;

struct ErrorState
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  enum
  {
    SIZE = 12
  };

  Matrix<double, SIZE, 1> arr;
  Map<Vector6d> X;
  Map<Vector3d> p;
  Map<Vector3d> q;
  Map<Vector3d> v;
  Map<Vector3d> w;

  ErrorState() :
    X(arr.data()),
    p(arr.data()),
    q(arr.data()+3),
    v(arr.data()+6),
    w(arr.data()+9)
  {}

  ErrorState(const ErrorState& obj) :
    X(arr.data()),
    p(arr.data()),
    q(arr.data()+3),
    v(arr.data()+6),
    w(arr.data()+9)
  {
    arr = obj.arr;
  }

  ErrorState& operator= (const ErrorState& obj)
  {
    arr = obj.arr;
    return *this;
  }

  ErrorState operator* (const double& s)
  {
    ErrorState out;
    out.arr = s * arr;
    return out;
  }

  ErrorState operator+ (const ErrorState& obj)
  {
    ErrorState out;
    out.arr = obj.arr + arr;
    return out;
  }
};

struct State
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  enum
  {
    SIZE = 13
  };

  Matrix<double, SIZE, 1> arr;
  Xformd X;
  Map<Vector3d> p;
  Quatd q;
  Map<Vector3d> v;
  Map<Vector3d> w;

  State() :
    X(arr.data()),
    p(arr.data()),
    q(arr.data()+3),
    v(arr.data()+7),
    w(arr.data()+10)
  {
    arr.setZero();
    q = Quatd::Identity();
  }

  State(const State& x) :
    X(arr.data()),
    p(arr.data()),
    q(arr.data()+3),
    v(arr.data()+7),
    w(arr.data()+10)
  {
    arr = x.arr;
  }

  State& operator= (const State& obj)
  {

    arr = obj.arr;
    return *this;
  }

  State operator+(const ErrorState& dx) const
  {
    State xp;
    xp.p = p + dx.p;
    xp.q = q + dx.q;
    xp.v = v + dx.v;
    xp.w = w + dx.w;
    return xp;
  }

  State& operator+=(const ErrorState& dx)
  {
    p = p + dx.p;
    q = q + dx.q;
    v = v + dx.v;
    w = w + dx.w;
    return *this;
  }

  ErrorState operator-(const State& x) const
  {
    ErrorState dx;
    dx.p = p - x.p;
    dx.q = q - x.q;
    dx.v = v - x.v;
    dx.w = w - x.w;
    return dx;
  }

  State toSnapConvention()
  {
    // Convert from world-frame velocity to body-frame velocity
    State x = *this;
    x.v = q.rotp(v);
    return x;
  }
};

inline double evalPoly(const std::vector<double>& coeffs, const double& x)
{
  // assumption: coeffs is ordered as [a_n ... a_0] s.t y = a_n*x^n + ... + a_0
  double y = 0.0;
  for (size_t i=0; i<coeffs.size(); ++i) {
    y += coeffs[i] * std::pow(x, coeffs.size() - 1 - i);
  }
  return y;
}

inline double sat(const double &val, const double &min, const double &max)
{
    if (val < min)
        return min;
    else if (val > max)
        return max;
    else
        return val;
}

struct Input
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  enum
  {
    SIZE = 4
  };
  Matrix<double, SIZE, 1> arr;
  double& T;
  Map<Vector3d> tau;

  Input() :
    T(*arr.data()),
    tau(arr.data()+3)
  {}
};

struct IMU
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  enum
  {
    SIZE = 6
  };
  Matrix<double, SIZE, 1> arr;
  Map<Vector3d> acc;
  Map<Vector3d> gyro;

  IMU() :
    acc(arr.data()),
    gyro(arr.data()+3)
  {}
};

const static Vector3d e_z(0., 0., 1.);

// Input indices
enum {
  THRUST = 0,
  TAUX = 1,
  TAUY = 2,
  TAUZ = 3,
  WX = 1,
  WY = 2,
  WZ = 3,
  INPUT_SIZE = 4
};

// IMU indices
enum {
  ACC = 0,
  GYRO = 3
};

static const Matrix3d M_ = [] {
  Matrix3d tmp;
  tmp << 1, 0, 0, 0, 1, 0, 0, 0, 0;
  return tmp;
}();

class RigidBodyDynamics
{
typedef Matrix<double, 12, 12> Matrix12d;
typedef Matrix<double, 12, 1> Vector12d;
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  RigidBodyDynamics();

  void loadParameters(const Matrix<double, 13, 1> &x0, const double& mass, const Matrix3d &inertia,
                      const double& g);
  void run(const double dt, const Vector4d& u,
           const Vector6d &ext_wrench=(Vector6d() << 0.,0.,0.,0.,0.,0.).finished());

  const State& get_state() const { return x_; }
  State& get_state() { return x_; }
  void set_state(const State& x) { x_ = x; }

  const Xformd& get_global_pose() const { return x_.X; }
  Vector3d get_imu_accel() const;
  Vector3d get_imu_gyro() const;

private:
  void f(const State& x, const Vector4d& ft, ErrorState& dx, const Vector6d &ext_wrench) const;
  void f(const State& x, const Vector4d& u, ErrorState& dx, Vector6d& imu, const Vector6d &ext_wrench) const;

  // States and RK4 Workspace
  State x_, x2_, x3_, x4_;
  ErrorState dx_, k1_, k2_, k3_, k4_;

  // Parameters
  double mass_;
  Eigen::Matrix3d inertia_matrix_, inertia_inv_;
  double g_;
  Vector6d imu_;
};

class DynamicsLite
{
public:
    DynamicsLite();
private:
    ros::NodeHandle nh_;
    ros::NodeHandle nh_private_;
    ros::Publisher  odom_pub_;
    ros::Publisher  pose_pub_;
    ros::Publisher  imu_pub_;
    ros::Subscriber motor_speeds_sub_;
    ros::Timer      timer_;
    tf2_ros::TransformBroadcaster tbr_;

    void loadParams();
    void actuatorsCallback(const mav_msgs::ActuatorsConstPtr &msg);
    void run(const ros::TimerEvent&);

    RigidBodyDynamics uav_;
    double grav_;
    double mass_;
    Matrix3d inertia_;
    Vector4d input_;
    double prev_time_sim_;
    double prev_time_motor_;
    bool taken_off_;
    Xformd x0_;
    std::vector<double> thrust_curve_;
    std::vector<double> torque_curve_;
    double tau_spin_up_;
    double tau_spin_down_;
    double min_thrust_;
    double max_thrust_;
    double min_torque_;
    double max_torque_;
    Vector3d com_;
    std::vector<double> motor_positions_;
    std::vector<double> motor_directions_;
    std::vector<double> motor_spin_;
    double gyro_stdev_;
    double gyro_bias_walk_stdev_;
    double accel_stdev_;
    double accel_bias_walk_stdev_;
    MatrixXd wrench_from_motor_thrust_map_;
    MatrixXd wrench_from_motor_torque_map_;
};
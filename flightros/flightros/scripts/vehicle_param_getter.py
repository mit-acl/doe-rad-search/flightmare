#!/usr/bin/python

import sys, yaml

def getParamFromCmd(param_def, cmd, idx=-1):    
    if cmd == 'get_veh_name':
        return param_def['vehicle_definitions'][idx]['name']
    elif cmd == 'get_veh_x':
        return param_def['vehicle_definitions'][idx]['initial_pose'][0]
    elif cmd == 'get_veh_y':
        return param_def['vehicle_definitions'][idx]['initial_pose'][1]
    elif cmd == 'get_veh_z':
        return param_def['vehicle_definitions'][idx]['initial_pose'][2]
    elif cmd == 'get_topic':
        topicnames = ['/%s/odometry' % veh_def['name'] for veh_def in param_def['vehicle_definitions']]
        return topicnames[idx]
    elif cmd == 'get_topic_list':
        topicnames = ['/%s/odometry' % veh_def['name'] for veh_def in param_def['vehicle_definitions']]
        return ','.join(topicnames)
    elif cmd == 'get_unity_id':
        return param_def['unity_scene_id']
    elif cmd == 'get_render':
        return param_def['render_unity']
    elif cmd == 'get_loop_freq':
        return param_def['main_loop_freq']
    elif cmd == 'get_cam_freq':
        return param_def['camera_freq']
    elif cmd == 'get_lid_freq':
        return param_def['lidar_freq']
    elif cmd == 'get_num_veh':
        return len(param_def['vehicle_definitions'])
    elif cmd == 'get_cam_topics':
        cam_topics = list()
        for veh_def in param_def['vehicle_definitions']:
            veh_id = veh_def['name']
            for i, cam_def in enumerate(veh_def['cameras']):
                cam_topics.append('%s/rgb/%d' % (veh_id, i))
        return cam_topics
    elif cmd == 'get_cam_frames':
        cam_frames = list()
        for veh_def in param_def['vehicle_definitions']:
            veh_id = veh_def['name']
            for i, cam_def in enumerate(veh_def['cameras']):
                if 'frame' in cam_def:
                    cam_frames.append(cam_def['frame'])
                else:
                    cam_frames.append('%s_cam%d' % (veh_id, i))
        return cam_frames
    elif cmd == 'get_depth_topics':
        depth_topics = list()
        for veh_def in param_def['vehicle_definitions']:
            veh_id = veh_def['name']
            for i, cam_def in enumerate(veh_def['cameras']):
                depth_topics.append('%s/depth/%d' % (veh_id, i))
        return depth_topics
    elif cmd == 'get_lid_topics':
        return ['/%s/lidar/pcl_file' % veh_def['name'] for veh_def in param_def['vehicle_definitions']]
    elif cmd == 'get_veh_poses':
        veh_poses = list()
        for veh_def in param_def['vehicle_definitions']:
            for pose_field in veh_def['initial_pose']:
                veh_poses.append(pose_field)                
        return veh_poses
    elif cmd == 'get_cam_poses':
        cam_poses = list()
        for veh_def in param_def['vehicle_definitions']:
            for camera in veh_def['cameras']:
                for cpose_field in camera['pose']:
                    cam_poses.append(cpose_field)
        return cam_poses
    elif cmd == 'get_cam_info':
        cam_info = list()
        for i, veh_def in enumerate(param_def['vehicle_definitions']):
            for camera in veh_def['cameras']:
                cam_info.append(i)
                cam_info.append(camera['width'])
                cam_info.append(camera['height'])
                cam_info.append(camera['fov'])
        return cam_info
    elif cmd == 'get_lid_info':
        lid_info = list()
        for i, veh_def in enumerate(param_def['vehicle_definitions']):
            if veh_def['lidar'] and 'lidar_range' in veh_def and 'lidar_resolution' in veh_def:
                lid_info.append(i)
                lid_info.append(veh_def['lidar_range'][0])
                lid_info.append(veh_def['lidar_range'][1])
                lid_info.append(veh_def['lidar_range'][2])
                lid_info.append(veh_def['lidar_resolution'])
        return lid_info
    elif cmd == 'get_num_obj':
        try:
            return len(param_def['object_definitions'])
        except KeyError:
            return 0
    elif cmd == 'get_obj_ids':
        if 'object_definitions' in param_def:
            return [obj['id'] for obj in param_def['object_definitions']]
        else:
            return []
    elif cmd == 'get_obj_pref':
        if 'object_definitions' in param_def:
            return [obj['prefab_id'] for obj in param_def['object_definitions']]
        else:
            return []
    elif cmd == 'get_obj_pos':
        obj_pos = list()
        if 'object_definitions' in param_def:
            for obj_def in param_def['object_definitions']:
                for pose_coeff in obj_def['pose']:
                    obj_pos.append(pose_coeff)
        return obj_pos
    elif cmd == 'get_obj_siz':
        obj_siz = list()
        if 'object_definitions' in param_def:
            for obj_def in param_def['object_definitions']:
                obj_siz.append(obj_def['scale']['x'])
                obj_siz.append(obj_def['scale']['y'])
                obj_siz.append(obj_def['scale']['z'])
        return obj_siz
    else:
        return ''

param_file = sys.argv[1]
cmd        = sys.argv[2]
if len(sys.argv) > 3:
    veh_idx = int(sys.argv[3])
else:
    veh_idx = -1

with open(param_file) as f:
    print(getParamFromCmd(yaml.load(f, Loader=yaml.Loader), cmd, veh_idx))


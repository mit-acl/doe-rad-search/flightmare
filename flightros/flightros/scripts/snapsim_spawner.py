#!/usr/bin/python

import rospy, yaml, rospkg, os
from subprocess import Popen

rospy.init_node('snapsim_spawner', anonymous=True)
param_file = rospy.get_param('~paramfile')
control_file = rospy.get_param('~controlfile')

log_dynamics = rospy.get_param('~log_dynamics')
dynamics_logdir = rospy.get_param('~dynamics_logdir')

launches = list()

with open(param_file) as f:
    param = yaml.load(f, Loader=yaml.Loader)

joy = int(param['joy'])
if param['planning']['architecture'] == 'apriori':
    apriori = 1
else:
    apriori = 0
rospack = rospkg.RosPack()
waypoint_fpath = os.path.join(rospack.get_path('trajectory_generation'),'waypoints','%s-%s.txt' % (param['planning']['method'],param['planning']['settings']))
flight_alt = param['planning']['flight_alt']

for i, vehicle in enumerate(param['vehicle_definitions']):
    index = vehicle['snap_idx']
    list_index = i
    odometry_topic = '/%s/odometry' %  vehicle['name']
    veh = vehicle['snap_veh']
    init_x = vehicle['initial_pose'][0]
    init_y = vehicle['initial_pose'][1]
    init_z = vehicle['initial_pose'][2]
    init_qw = vehicle['initial_pose'][3]
    init_qx = vehicle['initial_pose'][4]
    init_qy = vehicle['initial_pose'][5]
    init_qz = vehicle['initial_pose'][6]

    args = 'index:={} list_index:={} joy:={} odometry_topic:={} init_x:={} init_y:={} init_z:={} init_qw:={} init_qx:={} init_qy:={} init_qz:={} apriori:={} waypoint_fname:={} flight_alt:={}'.format(
                index, 
                list_index, 
                joy, 
                odometry_topic, 
                init_x, 
                init_y, 
                init_z,
                init_qw,
                init_qx,
                init_qy,
                init_qz,
                apriori,
                waypoint_fpath,
                flight_alt)
    if not control_file == "default":
        args += ' controlfile:={0} il_controlfile:={0} dynamicsfile:={0}'.format(control_file)
    if log_dynamics:
        args += ' log_dynamics:={} dynamics_logdir:={}'.format(log_dynamics, dynamics_logdir)
    launches.append('roslaunch flightros snap_sil_base.launch ' + args)

rospy.loginfo('[SNAPSIM_SPAWNER] Launching %d snap sim instances.' % len(launches))

processes = [Popen(cmd, shell=True) for cmd in launches]
# processes = [Popen(cmd) for cmd in launches]

try:
    for p in processes: p.wait()
except KeyboardInterrupt:
    rospy.loginfo('[SNAPSIM_SPAWNER] Shutting down.')
    for p in processes: p.kill()

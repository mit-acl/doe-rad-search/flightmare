#!/bin/bash

EXTPOSE="$1"
VEH="$2"
INDEX_STRING="$3"
IL_CONTROLFILE="$4"
ODOMETRY_TOPIC="$5"

roslaunch flightros snap_isolated.launch extpose:="${EXTPOSE}" veh:="${VEH}" \
  index_string:="${INDEX_STRING}" il_controlfile:="${IL_CONTROLFILE}" odometry_topic:="${ODOMETRY_TOPIC}" 2>/dev/null
#!/usr/bin/python

import rospy
from flightros_msgs.msg import SwarmStates
from nav_msgs.msg import Odometry

class EstimatesHub(object):
    def __init__(self):
        update_freq = rospy.get_param('~main_loop_freq', 50.0)
        num_vehicles = rospy.get_param('~num_vehicles', 1)

        self.swarmstate_msg = SwarmStates()
        self.swarmstate_msg.num_states = num_vehicles

        ivp_coeff = rospy.get_param('~initial_vehicle_poses', [])

        ivp_elem = 7
        sub_topics = rospy.get_param('~subscribe_topic_list').split(',')
        self.subscribers = list()
        
        for i in range(num_vehicles):
            odom_msg = Odometry()
            odom_msg.pose.pose.position.x    = ivp_coeff[ivp_elem*i+0]
            odom_msg.pose.pose.position.y    = ivp_coeff[ivp_elem*i+1]
            odom_msg.pose.pose.position.z    = ivp_coeff[ivp_elem*i+2]
            odom_msg.pose.pose.orientation.w = ivp_coeff[ivp_elem*i+3]
            odom_msg.pose.pose.orientation.x = ivp_coeff[ivp_elem*i+4]
            odom_msg.pose.pose.orientation.y = ivp_coeff[ivp_elem*i+5]
            odom_msg.pose.pose.orientation.z = ivp_coeff[ivp_elem*i+6]

            self.swarmstate_msg.states.append(odom_msg)

            sub_topic = sub_topics[i].strip()
            self.subscribers.append(rospy.Subscriber(sub_topic, Odometry, self.update_state, callback_args=(i), queue_size=1))

        self.pub = rospy.Publisher("flight_swarm/state_estimates", SwarmStates, queue_size=1)
        self.update_timer = rospy.Timer(rospy.Duration(1.0 / update_freq), self.publish)

    def update_state(self, msg, idx):
        self.swarmstate_msg.states[idx] = msg

    def publish(self, event):
        self.swarmstate_msg.header.stamp = rospy.Time.now()
        self.pub.publish(self.swarmstate_msg)

def main():
    # Initialize and clean up ROS node
    rospy.init_node('state_estimates_hub', anonymous=True)
    eh = EstimatesHub()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print 'Shutting down state estimates hub node.'

if __name__ == '__main__':
    main()
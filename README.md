# Flightmare

![Build Status](https://github.com/uzh-rpg/flightmare/workflows/CPP_CI/badge.svg) ![clang format](https://github.com/uzh-rpg/flightmare/workflows/clang_format/badge.svg)

*Tested on Ubuntu 18.04 with ROS Melodic.*

**Flightmare** is a flexible modular quadrotor simulator.
Flightmare is composed of two main components: a configurable rendering engine built on Unity and a flexible physics engine for dynamics simulation.
Those two components are totally decoupled and can run independently from each other.
Flightmare comes with several desirable features: (i) a large multi-modal sensor suite, including an interface to extract the 3D point-cloud of the scene; (ii) an API for reinforcement learning which can simulate hundreds of quadrotors in parallel; and (iii) an integration with a virtual-reality headset for interaction with the simulated environment.
Flightmare can be used for various applications, including path-planning, reinforcement learning, visual-inertial odometry, deep learning, human-robot interaction, etc.

**[Website](https://uzh-rpg.github.io/flightmare/)**, **[Unity source code](https://github.com/uzh-rpg/rpg_flightmare_unity)**

[![IMAGE ALT TEXT HERE](./docs/flightmare_main.png)](https://youtu.be/m9Mx1BCNGFU)

## Current State

Implemented features:

- Multi-agent integration with snap_sim
- Collision detection

Pending features:

- RGBD and LiDAR sensing
- Spawning Unity objects

## Installation

Required packages:

```bash
sudo apt-get update && sudo apt-get install -y --no-install-recommends \
build-essential \
cmake libzmqpp-dev \
libopencv-dev
```

Install ROS and Gazebo.

ROS dependencies:

```bash
sudo apt-get install libgoogle-glog-dev \
protobuf-compiler \
ros-$ROS_DISTRO-octomap-msgs \
ros-$ROS_DISTRO-octomap-ros \
ros-$ROS_DISTRO-joy \
python-vcstool
```

Use the command ```protoc --version``` to make sure that your protobuf compiler version is >= 3.0.0.

Install catkin tools:

```bash
sudo apt-get install python-pip
sudo pip install catkin-tools
```

Clone this repo into a catkin src directory, then build. Download the Flightmare Unity Binary **RPG\_Flightmare.tar.xz** from [this releases page](https://github.com/uzh-rpg/flightmare/releases) and extract it into *flightmare/flightrender*.

## Snap Sim Usage

Clone and build required snapstack repositories with

```bash
cd catkin_ws/src
vcs-import < flightmare/flightros/snap_dependencies.yaml
catkin build
```

For joystick operation, plug in a joystick and launch

```bash
roslaunch flightros snapsim_flightmare.launch
```

This will spawn two UAVs, both listening to commands from a single joystick.

See *flightmare/flightros/params/snapsim\_default.yaml* for the configurable parameters, including:

- Main loop and sensor rates
- Whether or not to render Unity
- Number of vehicles
- Vehicle initial poses
- Attached cameras and characteristics
- Attached LiDARs and characteristics (**PENDING**)

## Publication

If you use this code in a publication, please cite the following paper **[PDF](https://arxiv.org/abs/2009.00563)**

```
@article{yunlong2020flightmare,
  title={Flightmare: A Flexible Quadrotor Simulator},
  author={Song, Yunlong and Naji, Selim and Kaufmann, Elia and Loquercio, Antonio and Scaramuzza, Davide},
  journal={arXiv preprint arXiv:2009.00563},
  year={2020}
}
```

## License
This project is released under the MIT License. Please review the [License file](LICENSE) for more details.
